# Hello Stranger 👋
The purpose of this Repository is the following:
- Concentrate all Resources of Design and Web Design we create Sprint by Sprint
- Visibility through Code Tasks and HU's that not always are Code 🌱

# 🎨 Structure
```javascript
└─ /
  └─ logos: All Logos we create to some Screen, Component or for Our products in general
  └─ components: All atomic components such a Button, Table, Container, etc. 
  └─ views: Complete screens for features
```
